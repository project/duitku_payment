Duitku Payment for Drupal Commerce
=======================================================

Integrate your Drupal Commerce with Duitku payment gateway.

#### Description
This is the official Duitku module for the Drupal Commerce E-commerce platform.

#### Version Tested on
Drupal v9.4.5
Drupal Commerce v2.x.x
PHP v7.4.x

#### Instalation
Download the module then upload via FTP to your drupal website. 

1. [Download](https://docs.duitku.com/payment-gateway/plugin/#tab_duitkupop) the plugin file to your computer.
2. Using an FTP program, or your hosting control panel, upload the plugin folder to your Drupal modules installation's ``[Drupal folder]/modules/contrib/`` directory. Then, extract the plugin.
 >Please make sure the directory should be like ``[Drupal folder]/modules/contrib/duitku_payment``

#### Plugin Configuration
1. Open drupal admin page, open menu **Extend**.
2. Look for **Duitku Payment** modules under Payment Gateway group, enable by ticking the checkboxes.
3. Scroll down and click **Install**.
4. Go to **Commerce > Configuration > Payment > Payment gateways**.
5. Click **Add payment gateway**, click **Duitku** under Plugin Actions.
6. Fill the following config fields as instructed on each settings description.
7. Click save.
8. Now Duitku Payment should appear as a payment options when your customer checkout.

#### Advanced Usage
<details>
<summary>Note on Customer Data</summary>

##### Note on Customer Data
Unfortunately Drupal Commerce by default doesn't have `shipping address` and `phone number` as customer data <sup>\[1\]</sup>, so there will be no `phone` data passed to Duitku side.

These data is required for credit payment such as ATOME and IndoDana. If you'd like to add these payment method you need to modified your Drupal Commerce to have phone number input field. For shipping address you may create new input field or you could do just customize/edit this payment module set shipping address as billing address. Then you may have `shipping address` and `phone number` data to send.

You can uncomment this line:
```php
// 'phoneNumber' => $phoneNumber
// 'shippingAddress' => $address
```
Then modify it to something like this:
```php
'phoneNumber' => getPhoneNumber();
'shippingAddress' => getShippingAddress();
```
But you will need to figure out on your own, how to programmatically retrieve customer information based on your site implementation.

</details>

#### Get help
* [Duitku documentation](http://docs.duitku.com)
* Technical support [support@duitku.com](mailto:support@duitku.com)

#### Change Log

* 19 Sept 2022
  * init realease v1.0.0
