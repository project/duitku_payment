<?php

namespace Drupal\duitku_payment\Plugin\Commerce\PaymentGateway;

use Drupal;
use Drupal\commerce_order\Entity\OrderInterface;
use Drupal\commerce_payment\Exception\InvalidResponseException;
use Drupal\commerce_payment\Plugin\Commerce\PaymentGateway\OffsitePaymentGatewayBase;
use Drupal\commerce_price\Price;
use Drupal\Core\Form\FormStateInterface;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;

/**
 * Provides the QuickPay offsite Checkout payment gateway.
 *
 * @CommercePaymentGateway(
 *   id = "duitku",
 *   label = @Translation("Duitku Payment Gateway"),
 *   display_label = @Translation("Online Payment via Duitku"),
 *    forms = {
 *     "offsite-payment" = "Drupal\duitku_payment\PluginForm\PaymentOffsiteForm",
 *   },
 *   payment_method_types = {"credit_card"},
 *   credit_card_types = {
 *     "mastercard", "visa", "BCA", "Mandiri"
 *   },
 * )
 */

class Duitku extends OffsitePaymentGatewayBase {
    /**
    * {@inheritdoc}
    */
    public function defaultConfiguration() {
        return [
            'merchant_code' => '',
            'api_key' => '',
            'expiry_period' => '',
        ] + parent::defaultConfiguration();
    }

    /**
    * {@inheritdoc}
    */
    public function buildConfigurationForm(array $form, FormStateInterface $form_state) {
        $form = parent::buildConfigurationForm($form, $form_state);

        $form['merchant_code'] = [
            '#type' => 'textfield',
            '#title' => $this->t('Merchant Code'),
            '#description' => $this->t('This is the Merchant ID from the Duitku dashboard.'),
            '#default_value' => $this->configuration['merchant_code'],
            '#required' => TRUE,
        ];

        $form['api_key'] = [
            '#type' => 'textfield',
            '#title' => $this->t('Api Key'),
            '#description' => $this->t('This is the private key from the Duitku dashboard.'),
            '#default_value' => $this->configuration['api_key'],
            '#required' => TRUE,
        ];
        
        $form['expiry_period'] = [
            '#type' => 'number',
            '#title' => $this->t('Expiry Period'),
            '#description' => $this->t('Number as per minutes'),
            '#default_value' => $this->configuration['expiry_period'],
            '#required' => TRUE,
        ];

        return $form;
    }
    /**
     * {@inheritdoc}
     */
    public function submitConfigurationForm(array &$form, FormStateInterface $form_state) {
        parent::submitConfigurationForm($form, $form_state);
        if (!$form_state->getErrors()) {
            $values = $form_state->getValue($form['#parents']);
            $this->configuration['merchant_code'] = $values['merchant_code'];
            $this->configuration['api_key'] = $values['api_key'];
            $this->configuration['expiry_period'] = $values['expiry_period'];
        }

    }
    
    /**
     * {@inheritdoc}
     */
    protected function loadPaymentByOrderId($order_id) {
        /** @var \Drupal\commerce_payment\PaymentStorage $storage */
        $storage = $this->entityTypeManager->getStorage('commerce_payment');
        // $payment_by_order_id = $storage->load($order_id);
        $payment_by_order_id = $storage->loadByProperties(['order_id' => $order_id]);
        return reset($payment_by_order_id);
    }

    /**
     * {@inheritdoc}
     */
    public function onReturn(OrderInterface $order, Request $request)
    {
        // $payment = $this->loadPaymentByOrderId($order_id);
        $resultCode = $_GET['resultCode'];
        $merchantOrderId = $_GET['merchantOrderId'];
        $reference = $_GET['reference'];
        $resultStatus = "waiting for confirmation";

        if($resultCode === '01'){
            $resultStatus = "failed.";
        }
        
        $order->setData('state', 'pending');
        $order->save();
        
        $message = "<p><strong>Your payment status is '$resultStatus'</strong><br />";
        if($resultCode === '01'){
            $message .= "You can re-order again or contact the administrator of the website.";
        }else{
            $message .= "Here's your order id and refference. Just, in case you'll need this.";
            $message .= "Order id '$merchantOrderId'";
            $message .= "Refference '$reference'";
        }        
        $this->messenger()->addMessage($this->t($message));
    }

    /**
     * {@inheritdoc}
     */
    /** @var \Drupal\commerce_payment\Entity\PaymentGatewayInterface $onsite_gateway */
    /** @var \Drupal\commerce_order\Entity\OrderInterface $order */
    public function onNotify(Request $request)
    {
        $configuration = $this->getConfiguration();
        $merchantKey = $configuration['api_key'];
        $data = json_decode(json_encode($_REQUEST));
        
        \Drupal::logger('duitku')->debug('Duitku Callback: <pre>@callback</pre>', [
            '@callback' => var_export($_REQUEST, TRUE),
        ]);

        $merchantCode = $data->merchantCode; 
        $amount = $data->amount; 
        $merchantOrderId = $data->merchantOrderId; 
        $productDetail = $data->productDetail; 
        $additionalParam = $data->additionalParam; 
        $paymentMethod = $data->paymentCode; 
        $resultCode = $data->resultCode; 
        $merchantUserId = $data->merchantUserId; 
        $reference = $data->reference; 
        $signature = $data->signature; 

        if(!empty($merchantCode) && !empty($amount) && !empty($merchantOrderId) && !empty($signature))
        {
            $params = $merchantCode . $amount . $merchantOrderId . $merchantKey;
            $calcSignature = md5($params);

            if($signature == $calcSignature)
            {
                if($resultCode === '00'){
                    $transactionStatus = $this->checkTransaction($merchantOrderId);
                    if($transactionStatus == true){
                        $payment = Drupal\commerce_payment\Entity\Payment::create([
                            'type' => 'payment_default',
                            'payment_gateway' => 'duitku',
                            'order_id' => $merchantOrderId,
                            'amount' => new Price($amount, "IDR"),
                            'state' => 'completed',
                        ]);
                        $payment->save();
                        \Drupal::logger('duitku')->debug('Duitku Callback process: payment created', []);
                    }
                    echo 'success';
                    \Drupal::logger('duitku')->debug('Duitku Callback process: success', []);
                }else{
                    echo 'failed';
                    \Drupal::logger('duitku')->debug('Duitku Callback process: failed', []);
                }
            }
            else
            {
                throw new InvalidResponseException('Bad Signature.', 400);
                \Drupal::logger('duitku')->debug('Duitku Callback process: Bad Signature', []);
            }
        }
        else
        {
            throw new InvalidResponseException('Bad Parameter.', 400);
            \Drupal::logger('duitku')->debug('Duitku Callback: Bad Parameter', []);
        }
    }
    
    public function checkTransaction($order_id){
        $configuration = $this->getConfiguration();
        $merchantCode = $configuration['merchant_code']; // dari duitku
        $apiKey = $configuration['api_key']; // dari duitku
        $mode = $configuration['mode'];
        $merchantOrderId = $order_id; // dari anda (merchant), bersifat unik

        $signature = md5($merchantCode . $merchantOrderId . $apiKey);

        $params = array(
            'merchantCode' => $merchantCode,
            'merchantOrderId' => $merchantOrderId,
            'signature' => $signature
        );

        $params_string = json_encode($params);
        $url = 'https://api-sandbox.duitku.com/api/merchant/transactionStatus';
        if($mode !== 'test'){
            $url = 'https://api-prod.duitku.com/api/merchant/transactionStatus'; // Sandbox
        }
        $ch = curl_init();

        curl_setopt($ch, CURLOPT_URL, $url); 
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");                                                                     
        curl_setopt($ch, CURLOPT_POSTFIELDS, $params_string);                                                                  
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);                                                                      
        curl_setopt($ch, CURLOPT_HTTPHEADER, array(                                                                          
            'Content-Type: application/json',                                                                                
            'Content-Length: ' . strlen($params_string))                                                                       
        );   
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);

        //execute post
        $request = curl_exec($ch);
        $httpCode = curl_getinfo($ch, CURLINFO_HTTP_CODE);

        if($httpCode == 200)
        {
            $results = json_decode($request, true);
            // print_r($results, false);
            if($results['statusCode'] === '00'){
                return true;
            }else{
                return false;
            }
        }
        else
        {
            $request = json_decode($request);
            $error_message = "Server Error " . $httpCode ." ". $request->Message;
            throw new InvalidResponseException($error_message, $httpCode);
            \Drupal::logger('duitku')->debug('Duitku Callback: <pre>@error_message</pre>', [
                '@error_message' => var_export($error_message, TRUE),
            ]);
        }
    }
}