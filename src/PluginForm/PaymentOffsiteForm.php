<?php

namespace Drupal\duitku_payment\PluginForm;

use Drupal\commerce_payment\Exception\PaymentGatewayException;
use Drupal\commerce_payment\PluginForm\PaymentOffsiteForm as BasePaymentOffsiteForm;
use Drupal\Core\Form\FormStateInterface;

class PaymentOffsiteForm extends BasePaymentOffsiteForm {

  /**
   * {@inheritdoc}
   */
  public function buildConfigurationForm(array $form, FormStateInterface $form_state) {
    $form = parent::buildConfigurationForm($form, $form_state);


    /** @var \Drupal\commerce_payment\Entity\PaymentInterface $payment */
    $payment = $this->entity;
    $order = $payment->getOrder();

    /** @var \Drupal\commerce_payment\Plugin\Commerce\PaymentGateway\OffsitePaymentGatewayInterface $payment_gateway_plugin */
    $payment_gateway_plugin = $payment->getPaymentGateway()->getPlugin();
    $configuration = $payment_gateway_plugin->getConfiguration();
    $items = $order->getItems();
    
    // $callbackUrl = \Drupal::request()->getSchemeAndHttpHost() .  \Drupal::request()->getBasePath();
    // echo $host;
    // exit;

    /** @var \Drupal\address\Plugin\Field\FieldType\AddressItem $billingAddress */
    $billingAddress = $order->getBillingProfile()->get('address')->first();
        
    $mode = $configuration['mode'];
    $merchantCode = $configuration['merchant_code'];
    $merchantKey = $configuration['api_key'];

    $timestamp = round(microtime(true) * 1000); //in milisecond
    $paymentAmount = intval($payment->getAmount()->getNumber());
    $merchantOrderId = $payment->getOrderId();
    $productDetails = $order->getStore()->getName();
    $email = $order->getEmail();
    // $phoneNumber = '08123456789'; 
    $additionalParam = ''; // opsional
    $merchantUserInfo = ''; // opsional
    $customerVaName = '';
    if(empty($order->getCustomer()->getAccountName()) !== true){
      $merchantUserInfo = $order->getCustomer()->getAccountName();
      $customerVaName = $order->getCustomer()->getAccountName();
    }else{
      $merchantUserInfo = "Anonymous (not verified)";
      $customerVaName = $order->getEmail();
    };
    $callbackUrl = \Drupal::request()->getSchemeAndHttpHost() .  \Drupal::request()->getBasePath() . "/payment/notify/duitku"; // url untuk callback
    $returnUrl = $form['#return_url'];
    $expiryPeriod = intval($configuration['expiry_period']); // untuk menentukan waktu kedaluarsa dalam menit
    $signature = hash('sha256', $merchantCode.$timestamp.$merchantKey);

    // Detail pelanggan
    $firstName = $billingAddress->getGivenName();
    $lastName = $billingAddress->getFamilyName();

    // Alamat
    $alamat = $billingAddress->getAddressLine1() . ' ' . $billingAddress->getAddressLine2();
    $city = $billingAddress->getLocality();
    $postalCode = $billingAddress->getPostalCode();
    $countryCode = $billingAddress->getCountryCode();

    $address = array(
        'firstName' => $firstName,
        'lastName' => $lastName,
        'address' => $alamat,
        'city' => $city,
        'postalCode' => $postalCode,
        // 'phone' => $phoneNumber,
        'countryCode' => $countryCode
    );

    $customerDetail = array(
        'firstName' => $firstName,
        'lastName' => $lastName,
        'email' => $email,
        // 'phoneNumber' => $phoneNumber,
        'billingAddress' => $address,
        // 'shippingAddress' => $address
    );

    $itemDetails = array();
    foreach($items as $item){
      array_push($itemDetails, array(
        'name' => $item->getTitle(),
        'price' => intval($item->getTotalPrice()->getNumber()),
        'quantity' => intval($item->getQuantity()))
      );
    }

    $params = array(
        'paymentAmount' => $paymentAmount,
        'merchantOrderId' => $merchantOrderId,
        'productDetails' => $productDetails,
        'additionalParam' => $additionalParam,
        'merchantUserInfo' => $merchantUserInfo,
        'customerVaName' => $customerVaName,
        'email' => $email,
        // 'phoneNumber' => $phoneNumber,
        'itemDetails' => $itemDetails,
        'customerDetail' => $customerDetail,
        'callbackUrl' => $callbackUrl,
        'returnUrl' => $returnUrl,
        'expiryPeriod' => $expiryPeriod,
        //'paymentMethod' => $paymentMethod
    );

    $params_string = json_encode($params);
    $header = array(                                                                          
      'Content-Type: application/json',                                                                                
      'Content-Length: ' . strlen($params_string),
      'x-duitku-signature:' . $signature ,
      'x-duitku-timestamp:' . $timestamp ,
      'x-duitku-merchantcode:' . $merchantCode    
    );

    //var_dump($header);
    $url = 'https://api-sandbox.duitku.com/api/merchant/createInvoice'; // Sandbox
    if($mode !== 'test'){
      $url = 'https://api-prod.duitku.com/api/merchant/createInvoice'; // Sandbox
    }

    
    $ch = curl_init();

    curl_setopt($ch, CURLOPT_URL, $url); 
    curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");                                                                     
    curl_setopt($ch, CURLOPT_POSTFIELDS, $params_string);                                                                  
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);                                                                      
    curl_setopt($ch, CURLOPT_HTTPHEADER, $header);   
    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);

    //execute post
    $request = curl_exec($ch);
    $httpCode = curl_getinfo($ch, CURLINFO_HTTP_CODE);
    \Drupal::logger('duitku')->debug('Duitku Create Invoice Header: <pre>@header</pre>', [
      '@header' => var_export($header, TRUE),
    ]);
    \Drupal::logger('duitku')->debug('Duitku Create Invoice Body: <pre>@body</pre>', [
      '@body' => var_export($params_string, TRUE),
    ]);
    \Drupal::logger('duitku')->debug('Duitku Create Invoice Response: <pre>@response</pre>', [
      '@response' => var_export($request, TRUE),
    ]);
    
    $paymenturl = "";
    if($httpCode == 200)
    {
        $result = json_decode($request, true);
        $paymenturl .= $result['paymentUrl'];
    }
    else
    {
        echo $request ;
    }
    
    return $this->buildRedirectForm(
      $form,
      $form_state,
      $paymenturl,
      $params,
      self::REDIRECT_GET
    );
  }



}